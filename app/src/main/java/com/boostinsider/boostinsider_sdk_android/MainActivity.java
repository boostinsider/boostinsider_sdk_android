package com.boostinsider.boostinsider_sdk_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private ImageView imageView1;
    private ImageView imageView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView1 = (ImageView) findViewById(R.id.demo_image1);
        imageView2 = (ImageView) findViewById(R.id.demo_image2);

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent campaignOneActivity = new Intent(MainActivity.this, CampaignOneActivity.class);
                startActivity(campaignOneActivity);
            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent campaignTwoActivity = new Intent(MainActivity.this, CampaignTwoActivity.class);
                startActivity(campaignTwoActivity);
            }
        });
    }
}
