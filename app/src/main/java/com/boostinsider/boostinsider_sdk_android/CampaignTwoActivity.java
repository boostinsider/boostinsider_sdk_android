package com.boostinsider.boostinsider_sdk_android;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.boostinsider.android_sdk.Boostinsider;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by qiongchen on 3/11/16.
 */
public class CampaignTwoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaign2);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Campaign 2");
        actionBar.setDisplayHomeAsUpEnabled(true);

        Button button = (Button) findViewById(R.id.campaign2_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject payload = null;
                try {
                    payload = new JSONObject();
                    payload.put("campaign_id", 2);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Boostinsider.getInstance(getApplicationContext()).doCustomShare(CampaignTwoActivity.this, "Wonderful!!!", payload);
            }
        });
    }
}
