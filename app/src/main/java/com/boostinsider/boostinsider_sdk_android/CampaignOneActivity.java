package com.boostinsider.boostinsider_sdk_android;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.boostinsider.android_sdk.Boostinsider;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by qiongchen on 3/11/16.
 */
public class CampaignOneActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compaign1);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Campaign 1");
        actionBar.setDisplayHomeAsUpEnabled(true);

        Button button = (Button) findViewById(R.id.campaign1_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject payload = null;
                try {
                    payload = new JSONObject();
                    payload.put("campaign_id", 1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Boostinsider.getInstance(getApplicationContext()).doCustomShare(CampaignOneActivity.this, "Wonderful!!!", payload);
            }
        });

    }
}
