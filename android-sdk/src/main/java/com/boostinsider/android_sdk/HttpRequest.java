package com.boostinsider.android_sdk;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by qiongchen on 12/7/15.
 */
public class HttpRequest implements Runnable {

    public static final int CONNECT_TIMEOUT = 30000;
    public static final int READ_TIMEOUT = 30000;

    private String targetUrl;
    private String postData;

    private Boostinsider.OnServerResponseListener onServerResponseListener;

    public HttpRequest(String targetUrl, String postData, Boostinsider.OnServerResponseListener onServerResponseListener) {
        this.targetUrl = targetUrl;
        this.postData = postData;
        this.onServerResponseListener = onServerResponseListener;
    }

    @Override
    public void run() {
        doPostRequest(targetUrl, postData, onServerResponseListener);
    }

    public void doPostRequest(String targetUrl, String postData, Boostinsider.OnServerResponseListener onServerResponseListener) {
        int contentLength = postData.getBytes().length;

        HttpURLConnection connection = null;
        URL url;

        try {
            // create connection
            url = new URL(targetUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(CONNECT_TIMEOUT);
            connection.setReadTimeout(READ_TIMEOUT);
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            // make some HTTP header nicety
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Content-Length", contentLength + "");

            // send request
            DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
            writer.write(postData.getBytes());

            // clean up
            writer.flush();
            writer.close();

            // read the server response
            String line;
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }

            bufferedReader.close();

            // Handle the server response.
            onServerResponseListener.onServerResponse(null, stringBuilder.toString());

            Log.i(Boostinsider.BI_TAG, stringBuilder.toString());
        } catch (IOException e) {
            e.printStackTrace();
            onServerResponseListener.onServerResponse(e.toString(), null);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}