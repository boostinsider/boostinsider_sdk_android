package com.boostinsider.android_sdk;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by qiongchen on 12/16/15.
 */
public class EventQueue {

    public static final int SEND_SIZE = 5;

    private static EventQueue eventQueue;
    private static SharedPreferencesHelper sharedPreferencesHelper;

    private final List<String> eventList;

    private SystemInformation systemInformation;

    private EventQueue(Context context, SystemInformation systemInformation) {
        this.systemInformation = systemInformation;
        sharedPreferencesHelper = SharedPreferencesHelper.getInstance(context);
        eventList = createQueue();
    }

    public static EventQueue getInstance(Context context, SystemInformation systemInformation) {
        if (eventQueue == null) {
            eventQueue = new EventQueue(context, systemInformation);
        }

        return eventQueue;
    }

    private List<String> createQueue() {
        List<String> queue = Collections.synchronizedList(new LinkedList<String>());
        String listStr = sharedPreferencesHelper.getEventList();
        if (listStr != null && !listStr.isEmpty()) {
            try {
                JSONArray eventList = new JSONArray(listStr);
                for (int i = 0; i < eventList.length(); ++i) {
                    JSONObject event = eventList.getJSONObject(i);
                    queue.add(event.toString());
                }
            } catch (JSONException e) {
            }
        }
        return queue;
    }

    public int getEventListSize() {
        return eventList.size();
    }

    public boolean addEvent(String event) {
        boolean result = eventList.add(event);
        updateLocalStorage();
        return result;
    }

    public void clearEventList() {
        eventList.clear();
        sharedPreferencesHelper.setEventList(null);
    }

    public void updateLocalStorage() {
        List<String> newEventList = new ArrayList<>();
        for (String event: eventList) {
            if (event != null) {
                newEventList.add(event);
            }
        }
        sharedPreferencesHelper.setEventList(newEventList.toString());
    }

    public String generateEvent(String eventType, String eventContentStr) {
        JSONObject event = new JSONObject();

        try {
            JSONObject deviceInfo = new JSONObject();
            deviceInfo.put("carrier", sharedPreferencesHelper.getCarrier());
            deviceInfo.put("phone_model", sharedPreferencesHelper.getPhoneModel());
            deviceInfo.put("phone_brand", sharedPreferencesHelper.getPhoneBrand());
            deviceInfo.put("display", new JSONObject(sharedPreferencesHelper.getDisplay()));
            deviceInfo.put("product", sharedPreferencesHelper.getProduct());
            deviceInfo.put("simulator", sharedPreferencesHelper.getSimulator());

            JSONObject systemInfo = new JSONObject();
            systemInfo.put("language", sharedPreferencesHelper.getLanguage());
            systemInfo.put("country", sharedPreferencesHelper.getCountry());
            systemInfo.put("os_version", sharedPreferencesHelper.getOSVersion());
            systemInfo.put("os", sharedPreferencesHelper.getOS());

            JSONObject appInfo = new JSONObject();
            appInfo.put("app_version_name", sharedPreferencesHelper.getAppVersionName());
            appInfo.put("app_version_code", sharedPreferencesHelper.getAppVersionCode());
            appInfo.put("install_date", sharedPreferencesHelper.getInstallDate());
            appInfo.put("first_installation", sharedPreferencesHelper.getIsFirstInstall());

            JSONObject sdkInfo = new JSONObject();
            sdkInfo.put("sdk_version", sharedPreferencesHelper.getSDKVersion());

            JSONObject adInfo = new JSONObject();
            adInfo.put("ad_id", sharedPreferencesHelper.getAdId());

            event.put("device_id", sharedPreferencesHelper.getAndroidId());
            event.put("device_type", "Android");
            event.put("event_type", eventType);
            event.put("event_time", sharedPreferencesHelper.getSessionTime());
            event.put("package_name", sharedPreferencesHelper.getPackage());
            event.put("referrer", sharedPreferencesHelper.getReferrer());
            event.put("account_id", sharedPreferencesHelper.getUserId());
            event.put("device_info", deviceInfo);
            event.put("system_info", systemInfo);
            event.put("app_info", appInfo);
            event.put("sdk_info", sdkInfo);
            event.put("ad_info", adInfo);
            event.put("app_key", sharedPreferencesHelper.getAppKey());

            // session duration
            if (eventType.equals(Constant.BI_EVENT_CLOSE)) {
                long sessionDuration = sharedPreferencesHelper.getSessionTime() - sharedPreferencesHelper.getOpenTime();
                event.put("session_duration", sessionDuration);
            }

            // custom event content
            JSONObject eventContent;
            if (eventContentStr != null) {
                eventContent = new JSONObject(eventContentStr);
            } else {
                eventContent = null;
            }
            event.put("event_content", eventContent);
        } catch (JSONException e) {
        }

        if (eventType.equals(Constant.BI_EVENT_LOGOUT)) {
            sharedPreferencesHelper.setUserId(null);
        }

        return event.toString().replace("=", ":");
    }
}