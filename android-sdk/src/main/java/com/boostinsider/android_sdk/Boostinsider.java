package com.boostinsider.android_sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by qiongchen on 12/7/15.
 */
public class Boostinsider {

    public static final String BI_TAG = "com.boostinsider.sdk";

    //    public static final int SCHEDULER_DELAY = 60000;
    public static final int SCHEDULER_DELAY = 30000;  // 30000 for test, 60000 for live

    private static Boostinsider boostinsiderMain;

    private static SharedPreferencesHelper sharedPreferencesHelper;
    private static SystemInformation systemInformation;
    private static EventQueue eventQueue;
    private static EventsQueue eventsQueue;
    private static AppForegroundStateManager manager;

    private ScheduledExecutorService scheduledExecutorService;
    private ExecutorService executorService;
    private Future<?> future;
    private Context context;

//    private String splashActivity;

    private Boostinsider(Context context) {
        this.sharedPreferencesHelper = SharedPreferencesHelper.getInstance(context);
        this.systemInformation = new SystemInformation(context);
        this.eventQueue = EventQueue.getInstance(context, systemInformation);
        this.eventsQueue = EventsQueue.getInstance(context);
        this.manager = AppForegroundStateManager.getInstance();
        this.context = context;
        getExecutorService();
    }

    /**
     *
     * Singleton pattern
     *
     * @param context
     * @return
     */
    public static Boostinsider getInstance(Context context) {
        if (boostinsiderMain == null) {
            boostinsiderMain = new Boostinsider(context);
        }

        return boostinsiderMain;
    }

    /**
     *
     * When the app is launched, the timer will be started and it will send the data to the server
     * each one minute.
     *
     */
    public void init() {
        boostinsiderMain.setBoostinsiderActivityLifecycleInspector((Application) context);

        // Collect the system information when the app is launched.
        systemInformation.collectSystemInfo(new OnDataCollectedListener() {
            @Override
            public void onDataCollected() {
                manager.addListener(new AppForegroundStateManager.OnAppForegroundStateChangeListener() {
                    @Override
                    public void onAppForegroundStateChange(AppForegroundStateManager.AppForegroundState newState) {
                        if (AppForegroundStateManager.AppForegroundState.IN_FOREGROUND == newState && !sharedPreferencesHelper.getIsFirstInstall()) {
                            addEvent(Constant.BI_EVENT_OPEN_INSTALL, null);
                        } else if (AppForegroundStateManager.AppForegroundState.NOT_IN_FOREGROUND == newState) {
                            addEvent(Constant.BI_EVENT_CLOSE, null);
                        }
                    }
                });
            }
        });

        // Setup the scheduler that handles the post request by every one minute.
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if (eventQueue.getEventListSize() > 0) {
                    Log.i(BI_TAG, "1 minute");

                    eventsQueue.addConnection(sharedPreferencesHelper.getEventList());
                    eventQueue.clearEventList();
                    sendSession();
                } else {
                    Log.i(BI_TAG, "no event");
                    if (eventsQueue.getConnectionListSize() > 0) {
                        sendSession();
                    }
                }
            }
        }, SCHEDULER_DELAY, SCHEDULER_DELAY, TimeUnit.MILLISECONDS);
    }

    public String addEventInternal(String eventType, String customEvent) {
        sharedPreferencesHelper.setAppKey(systemInformation.getAppKey());

        long currentTime = systemInformation.getCurrentTime();
        sharedPreferencesHelper.setSessionTime(currentTime);

        if (eventType.equals(Constant.BI_EVENT_OPEN_INSTALL)) {
            sharedPreferencesHelper.setOpenTime(currentTime);
        }

        if (sharedPreferencesHelper.getInstallDate() == 0) {
            sharedPreferencesHelper.setIsFirstInstall(true);
            sharedPreferencesHelper.setInstallDate(systemInformation.getInstallDate());
        } else {
            sharedPreferencesHelper.setIsFirstInstall(false);
        }

        String event = eventQueue.generateEvent(eventType, customEvent);

        return event;
    }

    /**
     * add an event for the start session
     *
     * @param onServerResponseListener
     */
    public void addEventForStartSession(String event, OnServerResponseListener onServerResponseListener) {
        JSONObject content = null;
        String contentStr;
        try {
            content = new JSONObject();
            content.put("app_key", sharedPreferencesHelper.getAppKey());

            JSONObject systemInfo = new JSONObject();
            systemInfo.put("os_version", sharedPreferencesHelper.getOSVersion());
            systemInfo.put("os", sharedPreferencesHelper.getOS());
            content.put("system_info", systemInfo);

            JSONObject openEvent = new JSONObject(event);
            content.put("open_event", openEvent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        contentStr = content.toString().replace("=", ":");

        Log.i(BI_TAG, "start session event: " + contentStr);

        sendSession(contentStr, onServerResponseListener);
    }

    /**
     * add events for the normal session
     *
     * @param eventType
     * @param customEvent
     */
    public void addEvent(String eventType, String customEvent) {
        // Add the custom event in the event queue
        eventQueue.addEvent(addEventInternal(eventType, customEvent));

        Log.i(BI_TAG, "event count is: " + eventQueue.getEventListSize());

        // Send the event to the server if needed
        if (eventQueue.getEventListSize() >= EventQueue.SEND_SIZE) {
            // Add the events into the connection queue
            eventsQueue.addConnection(sharedPreferencesHelper.getEventList());
            eventQueue.clearEventList();
            sendSession();
        }
    }

    /**
     * send the start session
     *
     * @param content
     * @param onServerResponseListener
     */
    public void sendSession(String content, OnServerResponseListener onServerResponseListener) {
        String url = Constant.BI_BASE_URL + Constant.BI_START_SESSION_URL;
        future = executorService.submit(new HttpRequest(url, content, onServerResponseListener));
    }

    /**
     * send the normal session
     *
     */
    public void sendSession() {
        try {
            String url = Constant.BI_BASE_URL + Constant.BI_SAVE_EVENT_URL;
            String contentsStr = sharedPreferencesHelper.getConnectionList();

            JSONArray contents = new JSONArray(contentsStr);
            String contentStr = contents.get(0).toString();

            if (eventsQueue.getConnectionListSize() != 0 && (future == null || future.isDone())) {
                future = executorService.submit(new HttpRequest(url, contentStr, new OnServerResponseListener() {
                    @Override
                    public void onServerResponse(String err, String result) {
                        if (err != null) {
                            Log.i(BI_TAG, "err connection list size is: " + eventsQueue.getConnectionListSize());
                        } else {
                            try {
                                JSONObject response = new JSONObject(result);
                                int statusCode = response.getInt(Constant.BI_KEY_STATUS);
                                if (statusCode == 200) {
                                    eventsQueue.removeConnection();

                                    Log.i(BI_TAG, "200 connection list size is: " + eventsQueue.getConnectionListSize());
                                } else {
                                    String errorMessage = response.getString(Constant.BI_KEY_MESSAGE);
                                    Log.e(BI_TAG, errorMessage);

                                    Log.i(BI_TAG, "500 connection list size is: " + eventsQueue.getConnectionListSize());
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void createDeepLink(String chanel, JSONObject payload, OnServerResponseListener onServerResponseListener) {
        String url = Constant.BI_BASE_URL + Constant.BI_CREATE_DEEP_LINK;
        JSONObject content = null;
        String contentStr;
        String deviceId = sharedPreferencesHelper.getAndroidId();
        String userId = sharedPreferencesHelper.getUserId();

        Log.i(BI_TAG, payload.toString());

        try {
            content = new JSONObject();
            content.put("app_key", systemInformation.getAppKey());
            content.put("campaign", "");
            content.put("payload", payload);
            content.put("type", "app");
            content.put("chanel", chanel); // or a specified link
            content.put("user_id", userId);
            content.put("device_id", deviceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        contentStr = content.toString().replace("=", ":");

        future = executorService.submit(new HttpRequest(url, contentStr, onServerResponseListener));
    }

    public void getDeeplinkData(Activity activity, OnDeeplinkDataListener onDeeplinkDataListener) {
        Uri uri = activity.getIntent().getData();
        if (uri != null) {
            String queryStr = uri.getQueryParameter("data");
            onDeeplinkDataListener.onDeeplinkDataReceived(queryStr);
        } else {
            onDeeplinkDataListener.onDeeplinkDataReceived(null);
        }
    }

    public void onStartSession(OnServerResponseListener onServerResponseListener) {
        String event = addEventInternal(Constant.BI_EVENT_OPEN_INSTALL, null);
        addEventForStartSession(event, onServerResponseListener);
    }

    public void setUserLogin(String userId) {
        sharedPreferencesHelper.setUserId(userId);
        addEvent(Constant.BI_EVENT_LOGIN, null);
    }

    public void setUserLogout() {
        sharedPreferencesHelper.setUserId(null);
        addEvent(Constant.BI_EVENT_LOGOUT, null);
    }

    public void doCustomShare(FragmentActivity activity, String content, JSONObject payload) {
        DialogFragment dialogFragment = CustomShareFragment.newInstance(payload.toString(), content);
        dialogFragment.show(activity.getSupportFragmentManager(), "CustomShareFragment");
    }

    private void getExecutorService() {
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor();
        }
    }

    public void setBoostinsiderActivityLifecycleInspector(Application application) {
        BoostinsiderActivityLifecycleInspector inspector = new BoostinsiderActivityLifecycleInspector();
        application.unregisterActivityLifecycleCallbacks(inspector);
        application.registerActivityLifecycleCallbacks(inspector);
    }

    private class BoostinsiderActivityLifecycleInspector implements Application.ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            if (sharedPreferencesHelper.getPreviousActivity() != null && sharedPreferencesHelper.getPreviousActivity().equals(activity.getLocalClassName())) {
                addEvent(Constant.BI_EVENT_CLOSE, null);
            }
        }

        @Override
        public void onActivityStarted(Activity activity) {
            manager.onActivityVisible(activity);
        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {
            sharedPreferencesHelper.setPreviousActivity(activity.getLocalClassName());
        }

        @Override
        public void onActivityStopped(Activity activity) {
            manager.onActivityNotVisible(activity);
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    }

    public interface OnServerResponseListener {
        void onServerResponse(String err, String response);
    }

    public interface OnDataCollectedListener {
        void onDataCollected();
    }

    public interface OnDeeplinkDataListener {
        void onDeeplinkDataReceived(String data);
    }
}