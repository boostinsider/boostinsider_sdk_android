package com.boostinsider.android_sdk;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by qiongchen on 1/26/16.
 */
public class AppForegroundStateManager {

    private static final int BI_MESSAGE_NOTIFY_LISTENERS = 1;
    public static final long BI_APP_CLOSED_VALIDATION_TIME_IN_MS = 100; //0.1 Seconds
    private Reference<Activity> mForegroundActivity;
    private Set<OnAppForegroundStateChangeListener> mListeners = new HashSet<>();
    private AppForegroundState mAppForegroundState = AppForegroundState.NOT_IN_FOREGROUND;
    private NotifyListenersHandler mHandler;

    private static class SingletonHolder {
        public static final AppForegroundStateManager INSTANCE = new AppForegroundStateManager();
    }

    public static AppForegroundStateManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private AppForegroundStateManager() {
        mHandler = new NotifyListenersHandler(Looper.getMainLooper());
    }

    public enum AppForegroundState {
        IN_FOREGROUND,
        NOT_IN_FOREGROUND
    }

    public interface OnAppForegroundStateChangeListener {
        void onAppForegroundStateChange(AppForegroundState newState);
    }

    public void onActivityVisible(Activity activity) {
        if (mForegroundActivity != null) {
            mForegroundActivity.clear();
        }

        mForegroundActivity = new WeakReference<>(activity);
        determineAppForegroundState();
    }

    public void onActivityNotVisible(Activity activity) {
        if (mForegroundActivity != null) {
            Activity ref = mForegroundActivity.get();

            if (activity == ref) {
                mForegroundActivity.clear();
                mForegroundActivity = null;
            }
        }

        determineAppForegroundState();
    }

    public Boolean isAppInForeground() {
        return mAppForegroundState == AppForegroundState.IN_FOREGROUND;
    }

    private void determineAppForegroundState() {
        AppForegroundState oldState = mAppForegroundState;

        final boolean isInForeground = mForegroundActivity != null && mForegroundActivity.get() != null;
        mAppForegroundState = isInForeground ? AppForegroundState.IN_FOREGROUND : AppForegroundState.NOT_IN_FOREGROUND;

        if (mAppForegroundState != oldState) {
            validateThenNotifyListeners();
        }
    }

    public void addListener(@NonNull OnAppForegroundStateChangeListener listener) {
        mListeners.add(listener);
    }

    public void removeListener(OnAppForegroundStateChangeListener listener) {
        mListeners.remove(listener);
    }

    private void notifyListeners(AppForegroundState newState) {
        for (OnAppForegroundStateChangeListener listener : mListeners) {
            listener.onAppForegroundStateChange(newState);
        }
    }

    private void validateThenNotifyListeners() {
        if (mHandler.hasMessages(BI_MESSAGE_NOTIFY_LISTENERS)) {
            mHandler.sendEmptyMessage(BI_MESSAGE_NOTIFY_LISTENERS);
        } else {
            mHandler.sendEmptyMessageDelayed(BI_MESSAGE_NOTIFY_LISTENERS, BI_APP_CLOSED_VALIDATION_TIME_IN_MS);
        }
    }

    private class NotifyListenersHandler extends Handler {
        private NotifyListenersHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BI_MESSAGE_NOTIFY_LISTENERS:
                    notifyListeners(mAppForegroundState);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }
}
