package com.boostinsider.android_sdk;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by qiongchen on 1/22/16.
 */
public class CustomShareImageAdapter extends BaseAdapter {

    private Context context;

    private int[] platformIcons = {
            R.drawable.ic_bi_facebook,
            R.drawable.ic_bi_gmail,
            R.drawable.ic_bi_google_plus,
            R.drawable.ic_bi_line,
            R.drawable.ic_bi_message,
            R.drawable.ic_bi_messenger,
            R.drawable.ic_bi_pinterest,
            R.drawable.ic_bi_skype,
            R.drawable.ic_bi_tumblr,
            R.drawable.ic_bi_twitter,
            R.drawable.ic_bi_wechat,
            R.drawable.ic_bi_whatsapp
    };

    private String [] platformTitles = {
            "Facebook",
            "Gmail",
            "Google+",
            "Line",
            "Message",
            "Messenger",
            "Pinterest",
            "Skype",
            "Tumblr",
            "Twitter",
            "Wechat",
            "WhatsApp"
    };

    public CustomShareImageAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return platformIcons.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view;

        if (convertView == null) {
            view = layoutInflater.inflate(R.layout.card_platform_item, null);

            ImageView platformIcon = (ImageView) view.findViewById(R.id.platform_item_imageview);
            platformIcon.setImageResource(platformIcons[position]);

            TextView platformTitle = (TextView) view.findViewById(R.id.platform_item_title);
            platformTitle.setText(platformTitles[position]);
        } else {
            view = convertView;
        }

        return view;
    }
}