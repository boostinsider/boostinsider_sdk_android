package com.boostinsider.android_sdk;

import android.app.Application;

/**
 * Created by qiongchen on 12/11/15.
 */
public class BoostinsiderApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Boostinsider.getInstance(this).init();
    }
}
