package com.boostinsider.android_sdk;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by qiongchen on 12/18/15.
 */
public class EventsQueue {

    private static EventsQueue eventsQueue;
    private static SharedPreferencesHelper sharedPreferencesHelper;

    private final List<String> connectionList;

    private EventsQueue(Context context) {
        sharedPreferencesHelper = SharedPreferencesHelper.getInstance(context);
        connectionList = createQueue();
    }

    public static EventsQueue getInstance(Context context) {
        if (eventsQueue == null) {
            eventsQueue = new EventsQueue(context);
        }

        return eventsQueue;
    }

    private List<String> createQueue() {
        List<String> queue = Collections.synchronizedList(new LinkedList<String>());
        String listStr = sharedPreferencesHelper.getConnectionList();
        if (listStr != null && !listStr.isEmpty()) {
            try {
                JSONArray totalEventsList = new JSONArray(listStr);
                for (int i = 0; i < totalEventsList.length(); ++i) {
                    JSONArray eventsList = totalEventsList.getJSONArray(i);
                    for (int j = 0; j < eventsList.length(); ++j) {
                        JSONArray eventList = new JSONArray();
                        JSONObject event = eventsList.getJSONObject(j);
                        eventList.put(event);
                    }
                    queue.add(eventsList.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return queue;
    }

    public boolean addConnection(String eventContent) {
        boolean result = connectionList.add(eventContent);
        updateLocalStorage();
        return result;
    }

    public void clearConnectionList() {
        if (connectionList.size() != 0) {
            connectionList.clear();
        }
        sharedPreferencesHelper.setConnectionList(null);
    }

    public void updateLocalStorage() {
        List<String> newEventList = new ArrayList<>();
        for (String event: connectionList) {
            if (event != null) {
                newEventList.add(event);
            }
        }
        sharedPreferencesHelper.setConnectionList(newEventList.toString());
    }

    public int getConnectionListSize() {
        return connectionList.size();
    }

    /**
     *
     * Remove the first connection in the list. After the connection has been removed,
     * update the local storage.
     *
     */
    public void removeConnection() {
        connectionList.remove(0);

        if (connectionList.size() != 0) {
            updateLocalStorage();
        } else {
            clearConnectionList();
        }
    }
}
