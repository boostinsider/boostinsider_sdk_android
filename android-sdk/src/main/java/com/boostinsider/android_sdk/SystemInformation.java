package com.boostinsider.android_sdk;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Locale;

/**
 * Created by qiongchen on 12/7/15.
 */
public class SystemInformation {

    private Context context;
    private SharedPreferencesHelper sharedPreferencesHelper;

    public SystemInformation(Context context) {
        this.context = context;
        this.sharedPreferencesHelper = SharedPreferencesHelper.getInstance(context);
    }

    /**
     *
     * Get the current time in milliseconds
     *
     */
    public long getCurrentTime() {
        return System.currentTimeMillis();
    }

    /**
     *
     * Get the app version name.
     *
     */
    public String getAppVersionName() {
        String appVersionName = null;

        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }

        return appVersionName;
    }

    /**
     *
     * Get the app version code
     *
     */
    public String getAppVersionCode() {
        String appVersionCode = null;

        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersionCode = String.valueOf(packageInfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {

        }

        return appVersionCode;
    }

    /**
     *
     * Get the device's unique id.
     *
     */
    public String getDeviceId() {
        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return deviceId;
    }

    /**
     *
     * Get the package name of the app.
     *
     */
    public String getPackageName() {
        String packageName = context.getPackageName();
        return packageName;
    }

    /**
     *
     * Get the carrier.
     *
     */
    public String getCarrier() {
        String carrier = null;

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null && telephonyManager.getNetworkOperatorName() != null) {
            carrier = telephonyManager.getNetworkOperatorName();
        }

        return carrier;
    }

    /**
     *
     * Get the phone brand.
     *
     */
    public String getPhoneBrand() {
        String phoneBrand = Build.MANUFACTURER;
        return phoneBrand;
    }

    /**
     *
     * Get the phone model.
     *
     */
    public String getPhoneModel() {
        String phoneModel = Build.MODEL;
        return phoneModel;
    }

    /**
     *
     * Get the product.
     *
     */
    public String getProduct() {
        String product = Build.PRODUCT;
        return product;
    }

    /**
     *
     * Get the OS.
     *
     */
    public String getOS() {
        String os = "Android";
        return os;
    }

    /**
     *
     * Get the OS version.
     *
     */
    public String getOSVersion() {
        String osVersion = String.valueOf(Build.VERSION.RELEASE);
        return osVersion;
    }

    /**
     *
     * Get the is simulator.
     *
     */
    public boolean isSimulator() {
        boolean isSimulator = Build.FINGERPRINT.contains("generic");
        return isSimulator;
    }

    /**
     *
     * Get the display information.
     *
     */
    public DisplayInformation getDisplayInfo() {
        WindowManager windowManager = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE));
        Display display = windowManager.getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);

        int realWidth = displayMetrics.widthPixels;
        int realHeight = displayMetrics.heightPixels;

        if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17) {
            try {
                realWidth = (Integer) Display.class.getMethod("getRawWidth").invoke(display);
                realHeight = (Integer) Display.class.getMethod("getRawHeight").invoke(display);
            } catch (Exception e) {
            }
        } else if (Build.VERSION.SDK_INT >= 17) {
            try {
                Point realSize = new Point();
                Display.class.getMethod("getRealSize", Point.class).invoke(display, realSize);
                realWidth = realSize.x;
                realHeight = realSize.y;
            } catch (Exception e) {
            }
        }

        DisplayInformation displayInformation = new DisplayInformation(realHeight, realWidth, displayMetrics);
        return displayInformation;
    }

    /**
     *
     * Get the advertising id.
     *
     */
    public String getAdvertisingId() {
        String advertisingId = null;

        try {
            Class<?> AdvertisingIdClientClass = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
            Method getAdvertisingIdInfoMethod = AdvertisingIdClientClass.getMethod("getAdvertisingIdInfo", Context.class);
            Object adInfoObj = getAdvertisingIdInfoMethod.invoke(null, context);
            Method getIdMethod = adInfoObj.getClass().getMethod("getId");
            advertisingId = (String) getIdMethod.invoke(adInfoObj);
        } catch(IllegalStateException ex) {

        } catch(Exception ignore) {

        }

        return advertisingId;
    }

    /**
     *
     * Get the current os language of the device.
     *
     */
    public String getLanguage() {
        String language = Locale.getDefault().getDisplayLanguage();
        return language;
    }

    /**
     *
     * Get the country.
     *
     */
    public String getCountry() {
        String locale = context.getResources().getConfiguration().locale.getCountry();
        return locale;
    }

    /**
     *
     * Get the install date timestamp of the app.
     *
     * This date is reset when the app is uninstalled and reinstalled.
     *
     */
    public long getInstallDate() {
        long currentInstallDateL = 0;

        try {
            currentInstallDateL = context.getPackageManager().getPackageInfo(getPackageName(), 0).firstInstallTime;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return currentInstallDateL;
    }

    public String getAppKey() {
        String appKey = null;

        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = applicationInfo.metaData;
            appKey = bundle.getString("com.boostinsider.AppKey");
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(Boostinsider.BI_TAG, "Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e) {
            Log.e(Boostinsider.BI_TAG, "Failed to load meta-data, NullPointer: " + e.getMessage());
        }

        return appKey;
    }

    class DisplayInformation {
        String widthStr, heightStr, ppiStr, sizeStr;
        double x, y, d, s;

        public DisplayInformation(int w, int h, DisplayMetrics displayMetrics) {
            x = Math.pow((double) (w / displayMetrics.xdpi), 2);
            y = Math.pow((double) (h / displayMetrics.ydpi), 2);
            s = Math.sqrt(x + y);
            d = Math.sqrt(Math.pow(w, 2) + Math.pow(h, 2)) / s;

            DecimalFormat df = new DecimalFormat("#.#");
            df.setRoundingMode(RoundingMode.HALF_UP);

            DecimalFormat df2 = new DecimalFormat("#");
            df2.setRoundingMode(RoundingMode.HALF_UP);

            widthStr = String.valueOf(w);
            heightStr = String.valueOf(h);
            sizeStr = widthStr + "×" + heightStr;
            ppiStr = df2.format(d);
        }

        public String getWidth() {
            return widthStr;
        }
        public String getHeight() {
            return heightStr;
        }
        public String getPpi() {
            return ppiStr;
        }
        public String getSize() {
            return sizeStr;
        }
    }

    /**
     *
     * Collect the system information and save it into the local storage.
     *
     */
    public synchronized void collectSystemInfo(final Boostinsider.OnDataCollectedListener onDataCollectedListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String carrier = getCarrier();
                String phoneModel = getPhoneModel();
                String androidId = getDeviceId();
                String phoneBrand = getPhoneBrand();

                String height = getDisplayInfo().getHeight();
                String width = getDisplayInfo().getWidth();
                String ppi = getDisplayInfo().getPpi();
                String size = getDisplayInfo().getSize();

                JSONObject displayInfo = new JSONObject();
                try {
                    displayInfo.put("width", width);
                    displayInfo.put("height", height);
                    displayInfo.put("ppi", ppi);
                    displayInfo.put("size", size);

                } catch (JSONException e) {

                }
                String displayInfoStr = displayInfo.toString();

                String product = getProduct();
                String language = getLanguage();
                String country = getCountry();
                String osVersion = getOSVersion();
                String os = getOS();
                String appVersionName = getAppVersionName();
                String appVersionCode = getAppVersionCode();
                String packageName = getPackageName();
                String referrer = sharedPreferencesHelper.getReferrer();
                String adId = getAdvertisingId();
                String sdkVersion = "0.0.1";   //***
                boolean isSimulator = isSimulator();

                sharedPreferencesHelper.setCarrier(carrier);
                sharedPreferencesHelper.setPhoneModel(phoneModel);
                sharedPreferencesHelper.setAndroidId(androidId);
                sharedPreferencesHelper.setPhoneBrand(phoneBrand);
                sharedPreferencesHelper.setDisplay(displayInfoStr);
                sharedPreferencesHelper.setProduct(product);
                sharedPreferencesHelper.setLanguage(language);
                sharedPreferencesHelper.setCountry(country);
                sharedPreferencesHelper.setOSVersion(osVersion);
                sharedPreferencesHelper.setOS(os);
                sharedPreferencesHelper.setAppVersionName(appVersionName);
                sharedPreferencesHelper.setAppVersionCode(appVersionCode);
                sharedPreferencesHelper.setPackage(packageName);
                sharedPreferencesHelper.setReferrer(referrer);
                sharedPreferencesHelper.setAdId(adId);
                sharedPreferencesHelper.setSDKVersion(sdkVersion);
                sharedPreferencesHelper.setSimulator(isSimulator);

                onDataCollectedListener.onDataCollected();
            }
        }).start();
    }
}