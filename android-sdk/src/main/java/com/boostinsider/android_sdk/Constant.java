package com.boostinsider.android_sdk;

/**
 * Created by qiongchen on 12/7/15.
 */
public class Constant {
    public static final String BI_BASE_URL = "http://192.168.2.113:8086"; // OFFICE
//    public static final String BI_BASE_URL = "https://boost.guru"; // www.boostsdk.com

    public static final String BI_SAVE_EVENT_URL = "/api/app/event/save";
    public static final String BI_START_SESSION_URL = "/api/app/session/start";
    public static final String BI_CREATE_DEEP_LINK = "/api/app/deeplink/create";

    public static final String BI_KEY_DATA = "data";
    public static final String BI_KEY_STATUS = "status";
    public static final String BI_KEY_MESSAGE = "message";

    // event type
    public static final String BI_EVENT_OPEN_INSTALL = "open_install";
    public static final String BI_EVENT_CLOSE = "close";
    public static final String BI_EVENT_LOGIN = "login";
    public static final String BI_EVENT_SIGNUP = "register";
    public static final String BI_EVENT_LOGOUT = "logout";
    public static final String BI_EVENT_CUSTOM = "custom";
    public static final String BI_EVENT_NULL = "null";
}