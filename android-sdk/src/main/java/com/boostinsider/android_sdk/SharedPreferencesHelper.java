package com.boostinsider.android_sdk;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by qiongchen on 12/7/15.
 */
public class SharedPreferencesHelper {

    private static final String PREF_FILE = "pref_file";

    public static final String BI_KEY_CARRIER = "carrier_key";
    public static final String BI_KEY_PHONE_MODEL = "phone_model_key";
    public static final String BI_KEY_ANDROID_ID = "android_id_key";
    public static final String BI_KEY_PHONE_BRAND = "phone_brand_key";
    public static final String BI_KEY_DISPLAY = "display_key";
    public static final String BI_KEY_PRODUCT = "product_key";
    public static final String BI_KEY_SIMULATOR = "simulator_key";
    public static final String BI_KEY_LANGUAGE = "language_key";
    public static final String BI_KEY_COUNTRY = "country_key";
    public static final String BI_KEY_OS_VERSION = "os_version_key";
    public static final String BI_KEY_OS = "os_key";
    public static final String BI_KEY_APP_VERSION_NAME = "app_version_name_key";
    public static final String BI_KEY_APP_VERSION_CODE = "app_version_code_key";
    public static final String BI_KEY_INSTALL_DATE = "install_date_key";
    public static final String BI_KEY_PACKAGE = "package_key";
    public static final String BI_KEY_SDK_VERSION = "sdk_version_key";
    public static final String BI_KEY_REFERRER = "referrer_key";
    public static final String BI_KEY_AD_ID = "ad_id_key";
    public static final String BI_KEY_FIRST_INSTALL = "first_install_key";
    public static final String BI_KEY_CURRENT_TIME = "current_time_key";
    public static final String BI_KEY_PREVIOUS_ACTIVITY = "previous_activity_key";
    public static final String BI_KEY_USER_ID = "user_id_key";
    public static final String BI_KEY_EVENT_LIST = "event_list_key";
    public static final String BI_KEY_CONNECTION_LIST = "connection_list_key";
    public static final String BI_KEY_OPEN_TIME = "open_time_key";
    public static final String BI_KEY_APP_KEY = "app_key_key";

    private static SharedPreferencesHelper sharedPreferencesHelper;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private SharedPreferencesHelper(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    /**
     *
     * Singleton pattern
     *
     * @param context
     * @return
     */
    public static SharedPreferencesHelper getInstance(Context context) {
        if (sharedPreferencesHelper == null) {
            sharedPreferencesHelper = new SharedPreferencesHelper(context);
        }
        return sharedPreferencesHelper;
    }

    public void setCarrier(String carrier) {
        setString(BI_KEY_CARRIER, carrier);
    }

    public String getCarrier() {
        return getString(BI_KEY_CARRIER);
    }

    public void setPhoneModel(String phoneModel) {
        setString(BI_KEY_PHONE_MODEL, phoneModel);
    }

    public String getPhoneModel() {
        return getString(BI_KEY_PHONE_MODEL);
    }

    public void setAndroidId(String id) {
        setString(BI_KEY_ANDROID_ID, id);
    }

    public String getAndroidId() {
        return getString(BI_KEY_ANDROID_ID);
    }

    public void setPhoneBrand(String brand) {
        setString(BI_KEY_PHONE_BRAND, brand);
    }

    public String getPhoneBrand() {
        return getString(BI_KEY_PHONE_BRAND);
    }

    public void setDisplay(String display) {
        setString(BI_KEY_DISPLAY, display);
    }

    public String getDisplay() {
        return getString(BI_KEY_DISPLAY);
    }

    public void setProduct(String product) {
        setString(BI_KEY_PRODUCT, product);
    }

    public String getProduct() {
        return getString(BI_KEY_PRODUCT);
    }

    public void setSimulator(boolean simulator) {
        setBool(BI_KEY_SIMULATOR, simulator);
    }

    public boolean getSimulator() {
        return getBool(BI_KEY_SIMULATOR);
    }

    public void setLanguage(String language) {
        setString(BI_KEY_LANGUAGE, language);
    }

    public String getLanguage() {
        return getString(BI_KEY_LANGUAGE);
    }

    public void setCountry(String country) {
        setString(BI_KEY_COUNTRY, country);
    }

    public String getCountry() {
        return getString(BI_KEY_COUNTRY);
    }

    public void setOSVersion(String osVersion) {
        setString(BI_KEY_OS_VERSION, osVersion);
    }

    public String getOSVersion() {
        return getString(BI_KEY_OS_VERSION);
    }

    public void setOS(String os) {
        setString(BI_KEY_OS, os);
    }

    public String getOS() {
        return getString(BI_KEY_OS);
    }

    public void setAppVersionName(String appVersionName) {
        setString(BI_KEY_APP_VERSION_NAME, appVersionName);
    }

    public String getAppVersionName() {
        return getString(BI_KEY_APP_VERSION_NAME);
    }

    public void setAppVersionCode(String appVersionCode) {
        setString(BI_KEY_APP_VERSION_CODE, appVersionCode);
    }

    public String getAppVersionCode() {
        return getString(BI_KEY_APP_VERSION_CODE);
    }

    public void setInstallDate(long installDate) {
        setLong(BI_KEY_INSTALL_DATE, installDate);
    }

    public long getInstallDate() {
        return getLong(BI_KEY_INSTALL_DATE);
    }

    public void setPackage(String packageName) {
        setString(BI_KEY_PACKAGE, packageName);
    }

    public String getPackage() {
        return getString(BI_KEY_PACKAGE);
    }

    public void setSDKVersion(String sdkVersion) {
        setString(BI_KEY_SDK_VERSION, sdkVersion);
    }

    public String getSDKVersion() {
        return getString(BI_KEY_SDK_VERSION);
    }

    public void setReferrer(String referrer) {
        setString(BI_KEY_REFERRER, referrer);
    }

    public String getReferrer() {
        return getString(BI_KEY_REFERRER);
    }

    public void setAdId(String adId) {
        setString(BI_KEY_AD_ID, adId);
    }

    public String getAdId() {
        return getString(BI_KEY_AD_ID);
    }

    public void setIsFirstInstall(boolean isFirstInstall) {
        setBool(BI_KEY_FIRST_INSTALL, isFirstInstall);
    }

    public boolean getIsFirstInstall() {
        return getBool(BI_KEY_FIRST_INSTALL, true);
    }

    public void setSessionTime(long sessionTime) {
        setLong(BI_KEY_CURRENT_TIME, sessionTime);
    }

    public long getSessionTime() {
        return getLong(BI_KEY_CURRENT_TIME);
    }

    public void setPreviousActivity(String previousActivity) {
        setString(BI_KEY_PREVIOUS_ACTIVITY, previousActivity);
    }

    public String getPreviousActivity() {
        return getString(BI_KEY_PREVIOUS_ACTIVITY);
    }

    public void setUserId(String userId) {
        setString(BI_KEY_USER_ID, userId);
    }

    public String getUserId() {
        return getString(BI_KEY_USER_ID);
    }

    public void setEventList(String events) {
        setString(BI_KEY_EVENT_LIST, events);
    }

    public String getEventList() {
        return getString(BI_KEY_EVENT_LIST);
    }

    public void setConnectionList(String connections) {
        setString(BI_KEY_CONNECTION_LIST, connections);
    }

    public String getConnectionList() {
        return getString(BI_KEY_CONNECTION_LIST);
    }

    public void setOpenTime(long time) {
        setLong(BI_KEY_OPEN_TIME, time);
    }

    public long getOpenTime() {
        return getLong(BI_KEY_OPEN_TIME);
    }

    public void setAppKey(String appKey) {
        setString(BI_KEY_APP_KEY, appKey);
    }

    public String getAppKey() {
        return getString(BI_KEY_APP_KEY);
    }

    private void setString(String key, String value) {
        sharedPreferencesHelper.editor.putString(key, value).commit();
    }

    private void setBool(String key, boolean value) {
        sharedPreferencesHelper.editor.putBoolean(key, value).commit();
    }

    private void setLong(String key, long value) {
        sharedPreferencesHelper.editor.putLong(key, value).commit();
    }

    private String getString(String key) {
        return sharedPreferencesHelper.sharedPreferences.getString(key, null);
    }

    private boolean getBool(String key) {
        return sharedPreferencesHelper.sharedPreferences.getBoolean(key, false);
    }

    private boolean getBool(String key, boolean defaultValue) {
        return sharedPreferencesHelper.sharedPreferences.getBoolean(key, defaultValue);
    }

    private long getLong(String key) {
        return sharedPreferencesHelper.sharedPreferences.getLong(key, 0L);
    }
}