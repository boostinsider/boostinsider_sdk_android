package com.boostinsider.android_sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by qiongchen on 12/7/15.
 */
public class BoostinsiderReferrerReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String referrer = intent.getStringExtra("referrer");
        SharedPreferencesHelper.getInstance(context).setReferrer(referrer);
        Log.i(Boostinsider.BI_TAG, referrer);
    }
}