package com.boostinsider.android_sdk;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by qiongchen on 1/22/16.
 */
public class CustomShareFragment extends DialogFragment {

    private static final String BI_KEY_PAYLOAD = "payload_key";
    private static final String BI_KEY_CONTENT = "content_key";

    private JSONObject payload;
    private String content;

    private CustomShareImageAdapter adapter;

    private String [] packageNames = {
            "com.facebook.katana",
            "com.google.android.gm",
            "com.google.android.apps.plus",
            "jp.naver.line.android",
            "com.android.mms",
            "com.facebook.orca",
            "com.pinterest",
            "com.skype.raider",
            "com.tumblr",
            "com.twitter.android",
            "com.tencent.mm",
            "com.whatsapp"
    };

    private String [] platforms = {
            "Facebook",
            "Gmail",
            "Google Plus",
            "Line",
            "Message",
            "Messenger",
            "Pinterest",
            "Skype",
            "Tumblr",
            "Twitter",
            "Wechat",
            "WhatsApp"
    };

    public static DialogFragment newInstance(String payload, String content) {
        Bundle args = new Bundle();
        args.putString(BI_KEY_PAYLOAD, payload);
        args.putString(BI_KEY_CONTENT, content);

        DialogFragment fragment = new CustomShareFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new CustomShareImageAdapter(getActivity().getApplicationContext());
        try {
            payload = new JSONObject(getArguments().getString(BI_KEY_PAYLOAD));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        content = getArguments().getString(BI_KEY_CONTENT);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();

        View dialogView = layoutInflater.inflate(R.layout.dialog_custom_share, null);
        GridView gridView = (GridView) dialogView.findViewById(R.id.custom_share_gridview);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String packageName = packageNames[position];
                String platformName = platforms[position];

                Boostinsider.getInstance(getActivity().getApplicationContext()).createDeepLink(platformName, payload, new Boostinsider.OnServerResponseListener() {
                    @Override
                    public void onServerResponse(String err, String response) {
                        if (err != null) {

                        } else {
                            Log.i(Boostinsider.BI_TAG, "response is: " + response);
                            try {
                                JSONObject returnData = new JSONObject(response);
                                int statusCode = returnData.getInt(Constant.BI_KEY_STATUS);
                                if (statusCode == 200) {
                                    String deeplink = returnData.getString(Constant.BI_KEY_DATA);
                                    String finalContent = content + "\n" + deeplink;

                                    Intent sendIntent = new Intent();
                                    sendIntent.setAction(Intent.ACTION_SEND);
                                    sendIntent.putExtra(Intent.EXTRA_TEXT, finalContent);
                                    sendIntent.setType("text/plain");
                                    sendIntent.setPackage(packageName);
                                    getActivity().startActivity(sendIntent);
                                } else {
                                    String errorMsg = returnData.getString(Constant.BI_KEY_MESSAGE);
                                    Log.e(Boostinsider.BI_TAG, errorMsg);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        });

        builder.setView(dialogView)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });

        Dialog dialog = builder.create();
        Window window = dialog.getWindow();
        window.setGravity(Gravity.BOTTOM);


        return dialog;
    }
}
